import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Componente3Component } from './componente3/componente3.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Componente3Component],
  exports: [Componente3Component]
})
export class Modulo3Module { }
