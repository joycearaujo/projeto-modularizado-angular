import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Componente2Component } from './componente2/componente2.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Componente2Component],
  exports: [Componente2Component]
})
export class Modulo2Module { }
