import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-componente1',
  templateUrl: './componente1.component.html',
  styleUrls: ['./componente1.component.css']
})
export class Componente1Component implements OnInit {

  contas: any = [];

  @Output() enviarConta = new EventEmitter();

  submit(email: String) {
    /* console.log(email); */
    this.contas.push({email: email});

    this.enviarConta.emit(this.contas);
    console.log(this.contas);
  }

  constructor() {

   }

  ngOnInit() {
  }

}
